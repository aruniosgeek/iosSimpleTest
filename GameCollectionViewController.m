//
//  GameCollectionViewController.m
//  iosSimpleTest
//
//  Created by Synergy on 1/3/16.
//  Copyright (c) 2016 TestApp. All rights reserved.
//

#import "GameCollectionViewController.h"
#import "CustomGridLayout.h"

#import "GameOperations.h"



@interface GameCollectionViewController () 





@end

@implementation GameCollectionViewController

static NSString * const reuseIdentifier = @"Cell";
NSArray *gameImageViewsArray;
NSArray *shuffeledGameImages;
NSString *lastFlippedName;
int lastFlippedIndex;

NSIndexPath *lastIndexPath;

int lastFlippedId=0;
int currentFlippedId=0;
UICollectionViewCell *lastCell;


int shuffledImageId=0;

int randomUniqueNumbers[16]={1,3,5,7,9,2,4,6,8,10,13,15,11,12,14,16};


- (void)viewDidLoad {
    [super viewDidLoad];
    
     // Loding the images
     gameImageViewsArray = [GameOperations loadImages];
     shuffeledGameImages = [GameOperations shuffleImagesArray:gameImageViewsArray];
    lastFlippedName = [lastFlippedName initWithString:@"First"];
    lastIndexPath =[[NSIndexPath alloc] init];
    
    

    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
#warning Incomplete method implementation -- Return the number of sections
  
    NSLog(@"nuo of secionts");
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
#warning Incomplete method implementation -- Return the number of items in the section
    
    NSLog(@"entering no of items for item");
    return 16;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    // UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    
    NSLog(@"entering cell for item");
    
    
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    GameImage *gridImageView = (GameImage *)[cell viewWithTag:100];

    
//    NSLog(@"Now the unique id setting is %d",randomUniqueNumbers[indexPath.item]);
//    gridImageView.unqueId=randomUniqueNumbers[indexPath.item];
    
    
    GameImage *shuffImg =[shuffeledGameImages objectAtIndex:indexPath.item];
    
     NSLog(@"Now the unique id setting from shuffele is is %d",shuffImg.unqueId);
    
    
    [gridImageView setUnqueId:shuffImg.unqueId];
    
    
   // [gridImageView setName:shuffImg.name];
   
    
    UIImage *img = [UIImage imageNamed:@"card_bg.png"];
    
    
    [gridImageView setImage:img];
    
    
    CATransform3D rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, .0, 0.5, 0.5);
    cell.contentView.alpha = 0.8;
    cell.contentView.layer.transform = rotation;
    cell.contentView.layer.anchorPoint = CGPointMake(0, 0.5);
    
    [UIView animateWithDuration:.5
                     animations:^{
                         cell.contentView.layer.transform = CATransform3DIdentity;
                         cell.contentView.alpha = 1;
                         cell.contentView.layer.shadowOffset = CGSizeMake(0, 0);
                     } completion:^(BOOL finished) {
                     }];
    
    
    
    

    
    return cell;
    
    
}

#pragma mark <UICollectionViewDelegate>


 // Uncomment this method to specify if the specified item should be highlighted during tracking
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
     NSLog(@"Something is caling");
	return YES;
 }
 

/*
 // Uncomment this method to specify if the specified item should be selected
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
 return YES;
 }
 */

/*
 // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
 }
 
 */
 
 - (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
     
     NSLog(@"Hello Test1");
	return NO;
 }
 
 
 - (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
      
	
 }


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
   
    UICollectionViewCell* cell = [collectionView cellForItemAtIndexPath:indexPath];
    GameImage *gridImage = (GameImage*)[cell viewWithTag:100];
 
    GameImage *newImageView =  [shuffeledGameImages objectAtIndex:indexPath.item];
    
    gridImage.image =  [newImageView image];
 
    
    [gridImage setImgName:[newImageView imgName]];
    
    gridImage.unqueId=[newImageView unqueId];
    
    
    gridImage.isImageDone=[newImageView isImageDone];
   
    gridImage.contentMode = UIViewContentModeScaleAspectFit;
    
    [self flipImage:collectionView withImage:gridImage withIndex:indexPath];
    
}



-(void)flipImage :(UICollectionView*)collectionView withImage:(GameImage*)gridImage withIndex:(NSIndexPath *)indexPath{
   
    
    UICollectionViewCell* cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    
    
    NSLog(@"Image done %d",gridImage.isImageDone);
    
    NSLog(@"Last Fliped image Name is  is %@",lastFlippedName);
    NSLog(@"Currently Fliped image  id is %d",gridImage.unqueId);
    NSLog(@"Last  Fliped image  id is %d",lastFlippedId);
    
    
    
    
    if(gridImage.isImageDone || gridImage.unqueId==lastFlippedId)
    {
        
        NSLog(@"You are done now");
        return;
        
    }
    
    [self flipAnimation:cell:gridImage];
    
    NSString *nameOfimg = [gridImage imgName];
    NSLog(@"Name of the image  Flipped is%@ ",nameOfimg);
    
    if (lastFlippedId==0) {
        lastFlippedId=[gridImage unqueId];
        lastFlippedName =nameOfimg;
        lastFlippedIndex=indexPath.item;
        lastIndexPath=indexPath;
        
         NSLog(@"Image currently Flipped %@",lastFlippedName);
        
    }
    else
    {
        if ([[gridImage imgName] isEqualToString:lastFlippedName]) {
            
            [[shuffeledGameImages objectAtIndex:indexPath.item] setIsImageDone:YES];
            
            
          //  gridImage.isImageDone=YES;
            
            NSLog(@"Image is Done  Now  . So Close now and last flipped ones. ");
            
            // WE have to close two cells
            
           // [self imageIsDone:cell withImage:gridImage];
            
            [self imageIsDone:collectionView withIndexPath:indexPath withImage:gridImage];
            [self imageIsDone:collectionView withIndexPath:lastIndexPath withImage:gridImage];
            
            lastFlippedName=@"";
            
        }
        else
        {
            
            lastFlippedName=nameOfimg;
            
            lastFlippedId=[gridImage unqueId];
           
            
            lastCell = cell;
            
            NSLog(@"Image is not done So We have to flip last one");
            
            NSLog(@"Last IndexPath is %p",lastIndexPath);
             NSLog(@"Current IndexPath is %p",indexPath);
            
            
            [self flipLastCell:collectionView withInxdexPath:lastIndexPath withGridImage:gridImage];
             lastIndexPath=indexPath;
         
        }
        
        
        
    }
 
}

-(void)flipAnimation:(UICollectionViewCell*)cell:(GameImage*)gridImage{
    
    gridImage.contentMode = UIViewContentModeScaleAspectFit;
    [UIView animateWithDuration:.10
                          delay:0
                        options:(UIViewAnimationOptionAllowUserInteraction)
                     animations:^
     {
         
         [UIView transitionFromView:cell.contentView
                             toView:gridImage
                           duration:.5
                            options:UIViewAnimationOptionTransitionFlipFromRight
                         completion:nil];
     }
                     completion:^(BOOL finished)
     {
         
         NSLog(@"animation end");
     }
     ];
    
    
}

-(void)flipLastCell:(UICollectionView *)collectionView withInxdexPath:(NSIndexPath *)indexPath withGridImage:(GameImage *)gridImageNew{
    
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    NSLog(@"Calling in the animation in CELL with Done");
    
    GameImage *gridImage = (GameImage*)[cell viewWithTag:100];
    
    GameImage *shuffImg =[shuffeledGameImages objectAtIndex:indexPath.item];
    
    NSLog(@"Now the unique id setting from shuffele is is %d",shuffImg.unqueId);
    [gridImage setUnqueId:shuffImg.unqueId];
    UIImage *img = [UIImage imageNamed:@"card_bg.png"];
    [gridImage setImage:img];
    gridImage.contentMode = UIViewContentModeScaleAspectFit;
    [self flipAnimation:cell:gridImage];
    
    
}

-(void)imageIsDone:(UICollectionView*)collectionView withIndexPath:(NSIndexPath*)indexPath withImage:(GameImage*)gridImage{
    
    
    
    NSLog(@"Calling in the animation in CELL with Done");
    UICollectionViewCell* cell = [collectionView cellForItemAtIndexPath:indexPath];
    UIImage *img = [UIImage imageNamed:nil];
    
    
    [[shuffeledGameImages objectAtIndex:indexPath.item] setIsImageDone:YES];
    

    
    [gridImage setImage:img];
     gridImage.contentMode = UIViewContentModeScaleAspectFit;
    
    [self flipAnimation:cell:gridImage];
    
    
    
}


/*
 
 - (CGSize)collectionView:(UICollectionView *)collectionView
 layout:(UICollectionViewLayout *)collectionViewLayout
 sizeForItemAtIndexPath:(NSIndexPath *)indexPath
 {
 
 NSLog(@"Hello Testing Layout>>");
 CGRect screenRect = [[UIScreen mainScreen] bounds];
 CGFloat screenWidth = screenRect.size.width;
 float cellWidth = screenWidth / 10.0; //Replace the divisor with the column count requirement. Make sure to have it in float.
 CGSize size = CGSizeMake(cellWidth, cellWidth);
 
 return size;
 
 
 CGRect mainRect = [[UIScreen mainScreen] bounds];
 CGFloat mainWidth = mainRect.size.width;
 UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
 flowLayout.minimumLineSpacing = mainWidth/10;
 flowLayout.itemSize = CGSizeMake(mainWidth/4, mainWidth/4);
 }
 
 */









@end