//
//  StartGameViewController.h
//  iosSimpleTest
//
//  Created by Synergy on 1/3/16.
//  Copyright (c) 2016 TestApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameCollectionViewController.h"

@interface StartGameViewController : GameCollectionViewController

@property (strong, nonatomic) IBOutlet UIView *titleView;




@end
