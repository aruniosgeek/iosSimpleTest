//
//  StartGameViewController.m
//  iosSimpleTest
//
//  Created by Synergy on 1/3/16.
//  Copyright (c) 2016 TestApp. All rights reserved.
//

#import "StartGameViewController.h"
#import "GameCollectionViewController.h"
#import "CustomGridLayout.h"

#import "GameOperations.h"
@interface StartGameViewController ()

@end

@implementation StartGameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"JHelloooo");
     
    self.collectionView.bounces = YES;
    [self.collectionView setShowsHorizontalScrollIndicator:NO];
    [self.collectionView setShowsVerticalScrollIndicator:NO];
    
    
    CGRect mainRect = [[UIScreen mainScreen] bounds];
    CGFloat mainWidth = mainRect.size.width;
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.minimumLineSpacing = 10.0f;
    flowLayout.itemSize = CGSizeMake(mainWidth/5, mainWidth/4);
    
    self.collectionView.collectionViewLayout = flowLayout;
    
    
   
    
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.titleView.bounds];
    self.titleView.layer.masksToBounds = NO;
    self.titleView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.titleView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    self.titleView.layer.shadowOpacity = 0.5f;
    self.titleView.layer.shadowPath = shadowPath.CGPath;
    
    
    

    
  
    
    
}


/*

- (BOOL)prefersStatusBarHidden
{
    return NO;
}
 */

    
- (void)viewWillAppear {
   }
    




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/





- (UIView *)titleView {
    CGFloat navBarHeight = self.navigationController.navigationBar.frame.size.height;
    CGFloat width = 0.95 * self.view.frame.size.width;
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, navBarHeight)];
    
    UIImage *logo = [UIImage imageNamed:@"logo.png"];
    UIButton *logoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    CGFloat logoY = floorf((navBarHeight - logo.size.height) / 2.0f);
    [logoButton setFrame:CGRectMake(0, logoY, logo.size.width, logo.size.height)];
    [logoButton setImage:logo forState:UIControlStateNormal];
    
    UIImage *bubble = [UIImage imageNamed:@"logo.png"];
    UIImageView *bubbleView = [[UIImageView alloc] initWithImage:bubble];
    
    const CGFloat Padding = 5.0f;
    CGFloat bubbleX =
    logoButton.frame.size.width +
    logoButton.frame.origin.x +
    Padding;
    CGFloat bubbleY = floorf((navBarHeight - bubble.size.height) / 2.0f);
    CGRect bubbleRect = bubbleView.frame;
    bubbleRect.origin.x = bubbleX;
    bubbleRect.origin.y = bubbleY;
    bubbleView.frame = bubbleRect;
    
    [containerView addSubview:logoButton];
    [containerView addSubview:bubbleView];
    
    return containerView;
}


-(void)addLeftButton
{
    UIImage *buttonImage = [UIImage imageNamed:@"logo.png"];
    
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [aButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    aButton.frame = CGRectMake(0.0, 0.0, buttonImage.size.width,   buttonImage.size.height);
    
    UIBarButtonItem *aBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    
    [aButton addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
    
    [self.navigationItem setLeftBarButtonItem:aBarButtonItem];
}


@end
