//
//  GameCollectionViewController.h
//  iosSimpleTest
//
//  Created by Synergy on 1/3/16.
//  Copyright (c) 2016 TestApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameGridLayout.h"

@interface GameCollectionViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(nonatomic,assign) NSArray *gridImages;

@property (nonatomic) GameGridLayout *gameGridLayout;

@end
