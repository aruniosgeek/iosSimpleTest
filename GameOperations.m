//
//  GameOperations.m
//  iosSimpleTest
//
//  Created by Synergy on 2/3/16.
//  Copyright (c) 2016 TestApp. All rights reserved.
//

#import "GameOperations.h"
#import "GameImage.h"


@implementation GameOperations





+(NSArray *) loadImages{
    
    
    NSMutableArray *gameImageArray = [NSMutableArray array];
    
    for (int i = 0; i < 8; i++) {
        
        
        NSString *imgName = @"colour";
        NSString *imgFullName = [NSString stringWithFormat:@"%@%d.png",imgName,i+1];
       
       //imgFullName = [[imgFullName stringByAppendingFormat:@"%i",i+1]stringByAppendingString:@".png"];
        
        UIImage *image= [UIImage imageNamed:[[imgName stringByAppendingFormat:@"%d",i+1]stringByAppendingString:@".png"]];
        
        
        GameImage *gameImage = [[GameImage alloc] initWithImage:image];
        
        gameImage.imgName=imgFullName;
        gameImage.isImageDone=NO;
        gameImage.unqueId=i+1;
      
        [gameImageArray addObject:gameImage];
        NSLog(@" Final ImagesView  Array is %@",gameImageArray);
        
    }
    
        NSArray *finalGameImagesArray = [NSArray arrayWithArray:gameImageArray];
        
        return finalGameImagesArray;
        
        
}
    
    
    

+(NSArray *)  shuffleImagesArray:(NSArray*) images{
    
    NSMutableArray *tmpArray = [[NSMutableArray alloc]initWithArray:images];
   
    for (int i = 0; i < 8; i++)
    {
        GameImage *gimg= [images objectAtIndex:i];
        gimg.unqueId=i+8;
        NSLog(@"game name is %@",gimg.imgName);
        
        [tmpArray addObject:gimg];
       
    }
    
    for (id anObject in images)
    {
        NSUInteger randomPos = arc4random()%([tmpArray count]+1);
        [tmpArray insertObject:anObject atIndex:randomPos];
    }
    
   
    return [NSArray arrayWithArray:tmpArray];
    
    
    
    
}













@end
