//
//  GameOperations.h
//  iosSimpleTest
//
//  Created by Synergy on 2/3/16.
//  Copyright (c) 2016 TestApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameImage.h"


@interface GameOperations : NSObject


@property NSMutableArray *gameImages;

@property(assign,atomic) NSNumber *points;


-(void)flipImage :GameImage;
+(NSArray *) shuffleImagesArray:(NSArray *)images;


-(void) addGamePoints:NSNumber;
+(NSArray *) loadImages;


@end
