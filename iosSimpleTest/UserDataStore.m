//
//  UserDataStore.m
//  TodayLuckyNumber
//
//  Created by Synergy on 27/1/16.
//  Copyright (c) 2016 ArunApp. All rights reserved.
//

#import "UserDataStore.h"
#import  <UIKit/UIKit.h>

@implementation UserDataStore



#define kTitleKey       @"Title"
#define kRatingKey      @"Rating"


+ (void)deleteAllEntities:(NSString *)nameEntity
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:nameEntity];
    [fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject *object in fetchedObjects)
    {
        NSLog(@"deleting the object%@",[object valueForKey:@"name"]) ;
        [context deleteObject:object];
    }
    
    error = nil;
   
    
    if (![context save:&error]) {
        NSLog(@"Can't delete the recoreds! %@ %@", error, [error localizedDescription]);
    }
    
    NSLog(@"deleted the recoreds Successfully");
    
    
}

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}


+ (BOOL)saveData:(NSDictionary *)scoreCard withEntity: (NSString *)entityNameValue {
   
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Create a new managed object
    NSManagedObject *newData = [NSEntityDescription insertNewObjectForEntityForName:entityNameValue inManagedObjectContext:context];
    
    [newData setValue:[scoreCard valueForKey:@"name"] forKey:@"name"];
    [newData setValue:[scoreCard valueForKey:@"email"]  forKey:@"score"];
   
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        return NO;
    }
    
    
    NSLog(@"Saved Successfully! in User Data Store");
    
    return YES;
   
    
}

+(id) getDataFromStore:(NSString*)dataKey{

NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:dataKey];
return [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
}




-(void)textFieldDidChange :(UITextField *)theTextField{
    NSLog( @"text changed: %@", theTextField.text);
}





@end
