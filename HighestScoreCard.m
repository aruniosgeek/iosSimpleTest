//
//  HighestScoreCard.m
//  iosSimpleTest
//
//  Created by Synergy on 6/3/16.
//  Copyright (c) 2016 TestApp. All rights reserved.
//

#import "HighestScoreCard.h"

#import <QuartzCore/QuartzCore.h>
#import "ScoreCell.h"







@interface HighestScoreCard (){
    NSMutableArray *objects;
}
@end

@implementation HighestScoreCard

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}





- (void)viewDidLoad
{
    [super viewDidLoad];
    
    objects = [[NSMutableArray alloc]init];
    
    NSMutableDictionary *scoreCard=[[NSMutableDictionary alloc]init];
    
    for (int i=1; i<100; i++) {
       
        [scoreCard setObject:@"My Name " forKey:@"name"];
        [scoreCard setObject:@"My Score " forKey:@"score"];
        
        
        [objects addObject:scoreCard];
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    
    NSLog(@"objects size is %d",[objects count]);
    return [objects count];
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //1. Get the cell
    static NSString *CellIdentifier = @"ScoreCell";
    ScoreCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[ScoreCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        
        
       // cell.lblUserName.backgroundColor = [self colorFromIndex:indexPath.row];
      
      //  cell.lblScore.font = [UIFont fontWithName:@"Helvetica" size:8];
     //   cell.imageView.image = [UIImage imageNamed:@"colors.gif"];
        
        
        
        //2. Apply some text styles
      //  cell..textColor = [self colorFromIndex:indexPath.row];
       // cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:11];
       // cell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:8];
      //  cell.imageView.image = [UIImage imageNamed:@"colors.gif"];
        
    }
    
    //3. Setup the cell
    
    
    
    
    NSMutableDictionary *scoreCardObj =[objects objectAtIndex:indexPath.row];
    
    
    NSLog(@"My Score is %@",[scoreCardObj valueForKey:@"score"]);
    
    NSLog(@"My Name is Hello  %@",[scoreCardObj valueForKey:@"name"]);

    
    cell.lblScore.text =[scoreCardObj valueForKey:@"score"];
    cell.lblUserName.text=[scoreCardObj valueForKey:@"name"];
    
    return cell;
}

//This function is where all the magic happens
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    //1. Setup the CATransform3D structure
    CATransform3D rotation;
    rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
    rotation.m34 = 1.0/ -600;
    
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = rotation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    //!!!FIX for issue #1 Cell position wrong------------
    if(cell.layer.position.x != 0){
        cell.layer.position = CGPointMake(0, cell.layer.position.y);
    }
    
    //4. Define the final state (After the animation) and commit the animation
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}


//Helper function to get a random float
- (float)randomFloatBetween:(float)smallNumber and:(float)bigNumber {
    float diff = bigNumber - smallNumber;
    return (((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * diff) + smallNumber;
}

- (UIColor*)colorFromIndex:(int)index{
    UIColor *color;
    
    //Purple
    if(index % 3 == 0){
        color = [UIColor colorWithRed:0.93 green:0.01 blue:0.55 alpha:1.0];
        //Blue
    }else if(index % 3 == 1){
        color = [UIColor colorWithRed:0.00 green:0.68 blue:0.94 alpha:1.0];
        //Blk
    }else if(index % 3 == 2){
        color = [UIColor blackColor];
    }
    else if(index % 3 == 3){
        color = [UIColor colorWithRed:0.00 green:1.0 blue:0.00 alpha:1.0];
    }
    
    
    return color;
    
}








@end
