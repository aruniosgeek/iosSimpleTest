//
//  GameImage.h
//  iosSimpleTest
//
//  Created by Synergy on 2/3/16.
//  Copyright (c) 2016 TestApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameImage : UIImageView


@property (strong,nonatomic) NSMutableString *imgName;
@property (assign,nonatomic) int unqueId;
@property(assign,nonatomic) BOOL isImageFliped;
@property (assign,nonatomic) BOOL isImageDone;

@end
